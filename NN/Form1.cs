﻿//Gosone Nithisuwan
//CS 469- Digital Image Processing

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NN
{
    public partial class Form1 : Form
    {
        Random rand;
        //Input to Hidden
        double[] ih;
        double biash1;
        double biash2;

        //Hidden to Output
        double[] ho;
        double biaso;

        int iterations;
        double correct;

        public Form1()
        {
            rand = new Random();
            iterations = 1;

            InitializeComponent();
            InitializeWeights();
        }

        //Randomly initializes the weights of each node between -0.5 and 0.5
        private void InitializeWeights()
        {
            ih = new double[4];
            ho = new double[2];

            biash1 = 0;
            biash2 = 0;
            biaso = 0;

            for( int i = 0; i < ih.Length; i++) { ih[i] = rand.NextDouble() * 2 - 1; }
            for (int i = 0; i < ho.Length; i++) { ho[i] = rand.NextDouble() * 2 - 1; }
        }

        private void train_tb_TextChanged(object sender, EventArgs e)
        {
            iterations = Convert.ToInt32(train_tb.Text);
        }

        private void train_tb_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (!char.IsDigit(e.KeyChar) || e.KeyChar == 8);
        }

        private void train_btn_Click(object sender, EventArgs e)
        {
            train();
        }


        private void train()
        {
            double in1 = 1;
            double in2 = 1;

            correct = 0;

            for (int i = 0; i < iterations; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    forward(in1, in2, true);
                    cycletraining(in1, in2, out in1, out in2);
                }
                Console.WriteLine();
            }

            double accuracy = correct / (iterations * 4 );
            accuracy = accuracy * 100;
            accuracy_lbl.Text = accuracy.ToString();

            oh1_tb.Text = ho[0].ToString();
            oh2_tb.Text = ho[1].ToString();
            ih11_tb.Text = ih[0].ToString();
            ih12_tb.Text = ih[2].ToString();
            ih21_tb.Text = ih[1].ToString();
            ih22_tb.Text = ih[3].ToString();
        }

        private void cycletraining(double in1, double in2, out double newin1, out double newin2)
        {
            newin1 = 0;
            newin2 = 0;
            if( in1 == 1 && in2 == 1) { newin1 = 1; newin2 = -1; }
            if (in1 == 1 && in2 == -1) { newin1 = -1; newin2 = 1; }
            if (in1 == -1 && in2 == 1) { newin1 = -1; newin2 = -1; }
            if (in1 == -1 && in2 == -1) { newin1 = 1; newin2 = 1; }
        }

        private void forward(double in1, double in2, bool train)
        {
            double a = .8;

            double h1 = forwardInnerHidden(0, in1, in2, biash1);
            double h2 = forwardInnerHidden(2, in1, in2, biash2);

            h1 = forwardOuterHidden(h1, a);
            h2 = forwardOuterHidden(h2, a);

            double out1 = forwardInnerOutput(h1, h2);
            out1 = forwardOuterOutput(out1, a);

            if (train)
                backward(out1, h1, h2, in1, in2, a);
            else
                z_lbl.Text = out1.ToString();

            Console.WriteLine("{0} xor {1} = {2}", in1, in2, out1);

        }

        private double forwardInnerHidden(int index, double in1, double in2, double bias)
        {
            return (ih[index] * in1 + ih[index + 1] * in2) + bias;
        }

        private double forwardOuterHidden( double inner, double a)
        {
            return ( 2.0 / ( 1 + Math.Exp(a * -1 * inner ) ) ) - 1.0;
        }

        private double forwardInnerOutput(double in1, double in2)
        {
            return (ho[0] * in1 + ho[1] * in2) + biaso;
        }

        private double forwardOuterOutput(double inner, double a)
        {
            return (2.0 / (1 + Math.Exp(a * -1 * inner))) - 1.0;
        }

        private void backward(double z, double y1, double y2, double in1, double in2, double a)
        {
            //double eita = 6;
            double t = truthvalue(in1, in2);
            if ((z > 0) && (t > 0))
                correct++;
            else if ((z < 0) && (t < 0))
                correct++;

            double hoerror = (1.0 / 2.0) * (t - z) * (1.0 + z) * (1.0 - z);
            ho[0] = ho[0] + a * hoerror * y1;
            ho[1] = ho[1] + a * hoerror * y2;

            double h1error = (1.0 - y1) * (1.0 + y1) * 0.5 * hoerror * ho[0];
            double h2error = (1.0 - y2) * (1.0 + y2) * 0.5 * hoerror * ho[1];
            //ih[0] = oldih[0] + (a / 2.0)  * (t - z) * (1.0 + z) * (1.0 - z) * oldho[0] * (y1 + 1.0) * (y1 - 1.0) * in1;
            ih[0] = ih[0] + (a) * h1error * in1;
            ih[1] = ih[1] + (a) * h1error * in2;

            ih[2] = ih[2] + (a) * h2error * in1;
            ih[3] = ih[3] + (a) * h2error * in2;

            biaso +=  (a) * hoerror;
            biash1 +=  (a ) * h1error;
            biash2 += a * h2error;
        }

        /*
        private void backward(double z, double y1, double y2, double in1, double in2, double a)
        {
            double eita = 6;
            double t = truthvalue(in1, in2);
            ho[0] = ho[0] - eita * (a / 2.0) * (t - z) * (z + 1.0) * (z - 1.0) * y1;
            ho[1] = ho[1] - eita * (a / 2.0) * (t - z) * (z + 1.0) * (z - 1.0) * y2;

            ih[0] = ih[0] - eita * (a / 2.0) * (a / 2.0) * (t - z) * (z + 1.0) * (z - 1.0) * ho[0] * (y1 + 1.0) * (y1 - 1.0) * in1;
            ih[1] = ih[1] - eita * (a / 2.0) * (a / 2.0) * (t - z) * (z + 1.0) * (z - 1.0) * ho[0] * (y1 + 1.0) * (y1 - 1.0) * in2;

            ih[2] = ih[2] - eita * (a / 2.0) * (a / 2.0) * (t - z) * (z + 1.0) * (z - 1.0) * ho[1] * (y1 + 1.0) * (y1 - 1.0) * in1;
            ih[3] = ih[3] - eita * (a / 2.0) * (a / 2.0) * (t - z) * (z + 1.0) * (z - 1.0) * ho[1] * (y1 + 1.0) * (y1 - 1.0) * in2;

            //biaso = biaso + eita * (a / 2.0) * (t - z) * (z + 1.0) * (z - 1.0);
            //biash = biash + eita * (a / 2.0) * (a / 2.0) * (t - z) * (z + 1.0) * (z - 1.0) * ho[1] * (y1 + 1.0) * (y1 - 1.0);
        }*/

        private double truthvalue( double in1, double in2 )
        {
            if (in1 == in2)
                return -1;
            else
                return 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double in1 = 1;
            double in2 = 1;

            if( rad_11.Checked )
            {
                in1 = 1;
                in2 = 1;
            }
            else if( rad_1n1.Checked )
            {
                in1 = 1;
                in2 = -1;
            }
            else if( rad_n11.Checked )
            {
                in1 = -1;
                in2 = 1;
            }
            else if( rad_n1n1.Checked )
            {
                in1 = -1;
                in2 = -1;
            }

            forward(in1, in2, false);

            expected_lbl.Text = truthvalue(in1, in2).ToString();
        }

        private void reset_btn_Click(object sender, EventArgs e)
        {
            InitializeWeights();
            oh1_tb.Text = ho[0].ToString();
            oh2_tb.Text = ho[1].ToString();
            ih11_tb.Text = ih[0].ToString();
            ih12_tb.Text = ih[2].ToString();
            ih21_tb.Text = ih[1].ToString();
            ih22_tb.Text = ih[3].ToString();

            double accuracy = 0;
            accuracy_lbl.Text = accuracy.ToString();
            z_lbl.Text = "0";
            expected_lbl.Text = "0";
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
