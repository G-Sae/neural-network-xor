﻿namespace NN
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.train_btn = new System.Windows.Forms.Button();
            this.train_tb = new System.Windows.Forms.TextBox();
            this.z_lbl = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.reset_btn = new System.Windows.Forms.Button();
            this.title_lbl = new System.Windows.Forms.Label();
            this.h1_title_lbl = new System.Windows.Forms.Label();
            this.h2_title_lbl = new System.Windows.Forms.Label();
            this.o_title_lbl = new System.Windows.Forms.Label();
            this.in1_title_lbl = new System.Windows.Forms.Label();
            this.in2_title_lbl = new System.Windows.Forms.Label();
            this.weight_title_lbl = new System.Windows.Forms.Label();
            this.ih11_tb = new System.Windows.Forms.TextBox();
            this.ih21_tb = new System.Windows.Forms.TextBox();
            this.oh1_tb = new System.Windows.Forms.TextBox();
            this.oh2_tb = new System.Windows.Forms.TextBox();
            this.ih12_tb = new System.Windows.Forms.TextBox();
            this.ih22_tb = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.accuracy_lbl = new System.Windows.Forms.Label();
            this.rad_11 = new System.Windows.Forms.RadioButton();
            this.rad_1n1 = new System.Windows.Forms.RadioButton();
            this.rad_n11 = new System.Windows.Forms.RadioButton();
            this.rad_n1n1 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.expected_lbl = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.exit_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // train_btn
            // 
            this.train_btn.Location = new System.Drawing.Point(12, 48);
            this.train_btn.Name = "train_btn";
            this.train_btn.Size = new System.Drawing.Size(75, 23);
            this.train_btn.TabIndex = 0;
            this.train_btn.Text = "Train";
            this.train_btn.UseVisualStyleBackColor = true;
            this.train_btn.Click += new System.EventHandler(this.train_btn_Click);
            // 
            // train_tb
            // 
            this.train_tb.Location = new System.Drawing.Point(93, 50);
            this.train_tb.Name = "train_tb";
            this.train_tb.Size = new System.Drawing.Size(42, 20);
            this.train_tb.TabIndex = 1;
            this.train_tb.TextChanged += new System.EventHandler(this.train_tb_TextChanged);
            this.train_tb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.train_tb_KeyPress);
            // 
            // z_lbl
            // 
            this.z_lbl.AutoSize = true;
            this.z_lbl.Location = new System.Drawing.Point(207, 279);
            this.z_lbl.Name = "z_lbl";
            this.z_lbl.Size = new System.Drawing.Size(13, 13);
            this.z_lbl.TabIndex = 2;
            this.z_lbl.Text = "0";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(125, 340);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Test";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // reset_btn
            // 
            this.reset_btn.Location = new System.Drawing.Point(347, 47);
            this.reset_btn.Name = "reset_btn";
            this.reset_btn.Size = new System.Drawing.Size(75, 23);
            this.reset_btn.TabIndex = 6;
            this.reset_btn.Text = "Reset";
            this.reset_btn.UseVisualStyleBackColor = true;
            this.reset_btn.Click += new System.EventHandler(this.reset_btn_Click);
            // 
            // title_lbl
            // 
            this.title_lbl.AutoSize = true;
            this.title_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title_lbl.Location = new System.Drawing.Point(7, 9);
            this.title_lbl.Name = "title_lbl";
            this.title_lbl.Size = new System.Drawing.Size(234, 26);
            this.title_lbl.TabIndex = 7;
            this.title_lbl.Text = "Neural Network XOR";
            // 
            // h1_title_lbl
            // 
            this.h1_title_lbl.AutoSize = true;
            this.h1_title_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.h1_title_lbl.Location = new System.Drawing.Point(8, 188);
            this.h1_title_lbl.Name = "h1_title_lbl";
            this.h1_title_lbl.Size = new System.Drawing.Size(107, 17);
            this.h1_title_lbl.TabIndex = 8;
            this.h1_title_lbl.Text = "Hidden Node 1:";
            // 
            // h2_title_lbl
            // 
            this.h2_title_lbl.AutoSize = true;
            this.h2_title_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.h2_title_lbl.Location = new System.Drawing.Point(8, 218);
            this.h2_title_lbl.Name = "h2_title_lbl";
            this.h2_title_lbl.Size = new System.Drawing.Size(107, 17);
            this.h2_title_lbl.TabIndex = 9;
            this.h2_title_lbl.Text = "Hidden Node 2:";
            // 
            // o_title_lbl
            // 
            this.o_title_lbl.AutoSize = true;
            this.o_title_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.o_title_lbl.Location = new System.Drawing.Point(344, 149);
            this.o_title_lbl.Name = "o_title_lbl";
            this.o_title_lbl.Size = new System.Drawing.Size(101, 17);
            this.o_title_lbl.TabIndex = 10;
            this.o_title_lbl.Text = "Output Node 1";
            // 
            // in1_title_lbl
            // 
            this.in1_title_lbl.AutoSize = true;
            this.in1_title_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.in1_title_lbl.Location = new System.Drawing.Point(118, 149);
            this.in1_title_lbl.Name = "in1_title_lbl";
            this.in1_title_lbl.Size = new System.Drawing.Size(89, 17);
            this.in1_title_lbl.TabIndex = 11;
            this.in1_title_lbl.Text = "Input Node 1";
            // 
            // in2_title_lbl
            // 
            this.in2_title_lbl.AutoSize = true;
            this.in2_title_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.in2_title_lbl.Location = new System.Drawing.Point(227, 149);
            this.in2_title_lbl.Name = "in2_title_lbl";
            this.in2_title_lbl.Size = new System.Drawing.Size(89, 17);
            this.in2_title_lbl.TabIndex = 12;
            this.in2_title_lbl.Text = "Input Node 2";
            // 
            // weight_title_lbl
            // 
            this.weight_title_lbl.AutoSize = true;
            this.weight_title_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weight_title_lbl.Location = new System.Drawing.Point(20, 149);
            this.weight_title_lbl.Name = "weight_title_lbl";
            this.weight_title_lbl.Size = new System.Drawing.Size(67, 20);
            this.weight_title_lbl.TabIndex = 13;
            this.weight_title_lbl.Text = "Weights";
            // 
            // ih11_tb
            // 
            this.ih11_tb.Location = new System.Drawing.Point(121, 187);
            this.ih11_tb.Name = "ih11_tb";
            this.ih11_tb.ReadOnly = true;
            this.ih11_tb.Size = new System.Drawing.Size(76, 20);
            this.ih11_tb.TabIndex = 14;
            // 
            // ih21_tb
            // 
            this.ih21_tb.Location = new System.Drawing.Point(230, 185);
            this.ih21_tb.Name = "ih21_tb";
            this.ih21_tb.ReadOnly = true;
            this.ih21_tb.Size = new System.Drawing.Size(76, 20);
            this.ih21_tb.TabIndex = 15;
            // 
            // oh1_tb
            // 
            this.oh1_tb.Location = new System.Drawing.Point(347, 187);
            this.oh1_tb.Name = "oh1_tb";
            this.oh1_tb.ReadOnly = true;
            this.oh1_tb.Size = new System.Drawing.Size(76, 20);
            this.oh1_tb.TabIndex = 16;
            // 
            // oh2_tb
            // 
            this.oh2_tb.Location = new System.Drawing.Point(347, 217);
            this.oh2_tb.Name = "oh2_tb";
            this.oh2_tb.ReadOnly = true;
            this.oh2_tb.Size = new System.Drawing.Size(76, 20);
            this.oh2_tb.TabIndex = 17;
            // 
            // ih12_tb
            // 
            this.ih12_tb.Location = new System.Drawing.Point(121, 218);
            this.ih12_tb.Name = "ih12_tb";
            this.ih12_tb.ReadOnly = true;
            this.ih12_tb.Size = new System.Drawing.Size(76, 20);
            this.ih12_tb.TabIndex = 18;
            // 
            // ih22_tb
            // 
            this.ih22_tb.Location = new System.Drawing.Point(230, 217);
            this.ih22_tb.Name = "ih22_tb";
            this.ih22_tb.ReadOnly = true;
            this.ih22_tb.Size = new System.Drawing.Size(76, 20);
            this.ih22_tb.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Training Accuracy:";
            // 
            // accuracy_lbl
            // 
            this.accuracy_lbl.AutoSize = true;
            this.accuracy_lbl.Location = new System.Drawing.Point(105, 84);
            this.accuracy_lbl.Name = "accuracy_lbl";
            this.accuracy_lbl.Size = new System.Drawing.Size(13, 13);
            this.accuracy_lbl.TabIndex = 21;
            this.accuracy_lbl.Text = "0";
            // 
            // rad_11
            // 
            this.rad_11.AutoSize = true;
            this.rad_11.Location = new System.Drawing.Point(24, 277);
            this.rad_11.Name = "rad_11";
            this.rad_11.Size = new System.Drawing.Size(72, 17);
            this.rad_11.TabIndex = 22;
            this.rad_11.TabStop = true;
            this.rad_11.Text = " 1 XOR  1";
            this.rad_11.UseVisualStyleBackColor = true;
            // 
            // rad_1n1
            // 
            this.rad_1n1.AutoSize = true;
            this.rad_1n1.Location = new System.Drawing.Point(24, 300);
            this.rad_1n1.Name = "rad_1n1";
            this.rad_1n1.Size = new System.Drawing.Size(72, 17);
            this.rad_1n1.TabIndex = 23;
            this.rad_1n1.TabStop = true;
            this.rad_1n1.Text = " 1 XOR -1";
            this.rad_1n1.UseVisualStyleBackColor = true;
            // 
            // rad_n11
            // 
            this.rad_n11.AutoSize = true;
            this.rad_n11.Location = new System.Drawing.Point(24, 323);
            this.rad_n11.Name = "rad_n11";
            this.rad_n11.Size = new System.Drawing.Size(72, 17);
            this.rad_n11.TabIndex = 24;
            this.rad_n11.TabStop = true;
            this.rad_n11.Text = "-1 XOR  1";
            this.rad_n11.UseVisualStyleBackColor = true;
            // 
            // rad_n1n1
            // 
            this.rad_n1n1.AutoSize = true;
            this.rad_n1n1.Location = new System.Drawing.Point(24, 346);
            this.rad_n1n1.Name = "rad_n1n1";
            this.rad_n1n1.Size = new System.Drawing.Size(72, 17);
            this.rad_n1n1.TabIndex = 25;
            this.rad_n1n1.TabStop = true;
            this.rad_n1n1.Text = "-1 XOR -1";
            this.rad_n1n1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(122, 279);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "NN Output:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(122, 304);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Expect Output:";
            // 
            // expected_lbl
            // 
            this.expected_lbl.AutoSize = true;
            this.expected_lbl.Location = new System.Drawing.Point(207, 304);
            this.expected_lbl.Name = "expected_lbl";
            this.expected_lbl.Size = new System.Drawing.Size(13, 13);
            this.expected_lbl.TabIndex = 28;
            this.expected_lbl.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(214, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "If the training accuracy is less then 90, reset";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 412);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "Gosone Nithisuwan";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(141, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 31;
            this.label6.Text = "Iterations";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(346, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Reset Weights";
            // 
            // exit_btn
            // 
            this.exit_btn.Location = new System.Drawing.Point(370, 399);
            this.exit_btn.Name = "exit_btn";
            this.exit_btn.Size = new System.Drawing.Size(75, 23);
            this.exit_btn.TabIndex = 33;
            this.exit_btn.Text = "Exit";
            this.exit_btn.UseVisualStyleBackColor = true;
            this.exit_btn.Click += new System.EventHandler(this.exit_btn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 434);
            this.Controls.Add(this.exit_btn);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.expected_lbl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rad_n1n1);
            this.Controls.Add(this.rad_n11);
            this.Controls.Add(this.rad_1n1);
            this.Controls.Add(this.rad_11);
            this.Controls.Add(this.accuracy_lbl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ih22_tb);
            this.Controls.Add(this.ih12_tb);
            this.Controls.Add(this.oh2_tb);
            this.Controls.Add(this.oh1_tb);
            this.Controls.Add(this.ih21_tb);
            this.Controls.Add(this.ih11_tb);
            this.Controls.Add(this.weight_title_lbl);
            this.Controls.Add(this.in2_title_lbl);
            this.Controls.Add(this.in1_title_lbl);
            this.Controls.Add(this.o_title_lbl);
            this.Controls.Add(this.h2_title_lbl);
            this.Controls.Add(this.h1_title_lbl);
            this.Controls.Add(this.title_lbl);
            this.Controls.Add(this.reset_btn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.z_lbl);
            this.Controls.Add(this.train_tb);
            this.Controls.Add(this.train_btn);
            this.MaximumSize = new System.Drawing.Size(468, 473);
            this.MinimumSize = new System.Drawing.Size(467, 472);
            this.Name = "Form1";
            this.Text = "XOR Neural Network";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button train_btn;
        private System.Windows.Forms.TextBox train_tb;
        private System.Windows.Forms.Label z_lbl;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button reset_btn;
        private System.Windows.Forms.Label title_lbl;
        private System.Windows.Forms.Label h1_title_lbl;
        private System.Windows.Forms.Label h2_title_lbl;
        private System.Windows.Forms.Label o_title_lbl;
        private System.Windows.Forms.Label in1_title_lbl;
        private System.Windows.Forms.Label in2_title_lbl;
        private System.Windows.Forms.Label weight_title_lbl;
        private System.Windows.Forms.TextBox ih11_tb;
        private System.Windows.Forms.TextBox ih21_tb;
        private System.Windows.Forms.TextBox oh1_tb;
        private System.Windows.Forms.TextBox oh2_tb;
        private System.Windows.Forms.TextBox ih12_tb;
        private System.Windows.Forms.TextBox ih22_tb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label accuracy_lbl;
        private System.Windows.Forms.RadioButton rad_11;
        private System.Windows.Forms.RadioButton rad_1n1;
        private System.Windows.Forms.RadioButton rad_n11;
        private System.Windows.Forms.RadioButton rad_n1n1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label expected_lbl;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button exit_btn;
    }
}

